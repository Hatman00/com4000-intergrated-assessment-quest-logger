Context context = this;
        String text = null;
        File file = new File(context.getFilesDir(), "quests.txt");
        if (!file.exists()){
            try {
                file.createNewFile();
            }
            catch (Exception error){
                System.out.println("An error occurred creating quests.txt!");
                error.printStackTrace();
            }
        }
        try {
            FileWriter filewrite = new FileWriter(file);
            filewrite.write("This should work... But I doubt it");
            filewrite.close();
            Scanner fileRead = new Scanner(file);
            text = fileRead.nextLine();
            TextView textthing = findViewById(R.id.test);
            textthing.setText(text);
            fileRead.close();
        }
        catch (Exception error){
            System.out.println("An error occurred reading the file");
            error.printStackTrace();
        }