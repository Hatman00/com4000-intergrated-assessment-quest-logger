Hello!
Thank you for reading this file.
I just wanted to request that you name your commits clearly (As in what the commit changed) and
with correct grammar such as capital letters and the like.

Clear names will help me know what you have changed and save us both time.

(Sorry if this sounds too patronizing! I do not intend it to!)

Also, feel free to create your own branches of this. Just know that if you want to add any changes you make to your own branch
you will need to create a merge request.

Thank you for reading this if you did. ^_^