#This file contains information to connect to the database on a localhost machine. (NOT THROUGH DOCKER!)
db_config = {
    'user': 'root',
    'password': 'scooters',
    'host': 'localhost',
    'port': '1234',
    'database': 'scooterstack'
}