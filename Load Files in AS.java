//The normal way to load files in Java does not work in Android Studio as the filesystem of Android phones is read only.
//To get around this, Android creates a directory in the phone's internal storage which can be used by the app.
//To use it, do something like this:

/*Note, this requries importing the following:
"android.content.Context" 
"java.io.File" To open files
"java.io.FileWriter" To write to files
"java.io.Scanner" To read files
*/
public class MainActivity extends AppCompatActivity { //Note: Do not try to run this file. The class does not match the file name and thus will cause compiling to fail.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context context = this; //Sets a context variable that is equal to this class. (Tihs class being "MainActivity")
        File file = new File(context.getFilesDir(), "quests.txt"); //Creates a file object that holds the file "quest.txt" inside the context directory. It does not open the file however.
        if (!file.exists()){ //Cheks to see if the file object has the file specified. ("quests.txt")
            try {
                file.createNewFile(); //If the file does not exist, create it.
            }
            catch (Exception error){
                System.out.println("An error occurred creating quests.txt!"); //If something goes wrong with creating the file, do this instead to avoid crashing.
                error.printStackTrace();
            }
        }
        try {
            FileWriter filewrite = new FileWriter(file); //Open the file for writing
            filewrite.write("This should work... But I doubt it"); //Write the string to the file
            filewrite.close(); //Close the file for writing. Very important to avoid corruption!
            Scanner fileRead = new Scanner(file); //Open the file for reading
            String text = fileRead.nextLine(); //Read the FIRST line of the file and save it as a variable. 
            textthing.setText(text);
            fileRead.close(); //Close the file for reading. Also very important.
        }
        catch (Exception error){
            System.out.println("An error occurred reading the file"); //If reading the file failed, run this code to avoid crashing. 
            error.printStackTrace();
        }
    }
}