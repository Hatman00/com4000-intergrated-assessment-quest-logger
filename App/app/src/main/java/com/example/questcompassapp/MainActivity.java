package com.example.questcompassapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {
    LinearLayout buttonLinear; //Here we load the buttonLinear object and buttonScroll as their respective types.
    ScrollView buttonScroll;
    Context MainContext = this;
    public static final String EXTRA_MESSAGE = "com.example.questcompassapp.MESSAGE";
    View.OnClickListener questButtonPressed = new View.OnClickListener() { //Create a listener that runs the onClick function when a button is pressed. (Each button needs to have the listener set to it! EG: button.setOnClickListener(questButtonPressed))
        @Override
        public void onClick(View view) {
            String calling_button_id = String.valueOf(view.getId()); //Get the id of the butto that has been pressed. (The id in this case is the same as the quest it displays)
            System.out.println("You pressed a quest button with the ID of " + calling_button_id + "!");
            Intent intentQuestViewSwitch = new Intent(MainContext, QuestViewActivity.class); //Create intent to link this file and the QuestViewActivity
            intentQuestViewSwitch.putExtra(EXTRA_MESSAGE, calling_button_id); //Add the button id to the intent as a message.
            startActivity(intentQuestViewSwitch); //Move to the QuestViewActivity file and send the message.
            finish(); //Finish this activity.
        }
    };
    public boolean isInt(String numberString){ //Check if a string is an integer. It works by attempting to convert the string to an int which, if successful, will return true. If it fails, it returns false.
        try {
            Integer.parseInt(numberString);
            return true;
        }
        catch (Exception error){
            return false;
        }
    }

    public int getQuestAmount(File questFile){ //Get the amount of quests in "quests.txt" in order to decide how many buttons should be created.
        int questAmount = 0;
        String current_line = "";
        try {
            Scanner questScan = new Scanner(questFile);
            while (questScan.hasNextLine()) {
                current_line = questScan.nextLine();
                if (current_line.equals("StartQuest")) {
                    questAmount++;
                }
            }
        }
        catch (Exception error){
            error.printStackTrace();
        }
        System.out.println("Quest amount is: " + String.valueOf(questAmount));
        return questAmount;
    }

    public void readQuests(){
        String button_id = "questButton_"; //Constant for the ID of each button. The actual id is added after the underscore.
        String questID = ""; //String that temporarily holds the questID of each quest. (As once a line is read, the program moves to the next)
        String questName = ""; //String that temporarily holds the questName of each quest. (Ditto)
        String questDescription = ""; //String that temporarily holds the questDescription of each quest. (Ditto)
        String questStatus = ""; //String that temporarily holds the questStatus of each quest. (Ditto)
        buttonLinear = (LinearLayout) findViewById(R.id.main_linear_button_list); //Creates an object of the linear layout in the main screen as a LinearLayout object rather than a view object.
        buttonScroll = (ScrollView) findViewById(R.id.main_scrollview_button_list_scroll); //Ditto but it creates it as a scroll view instead.
        ViewGroup.LayoutParams buttonLinearParams = buttonLinear.getLayoutParams(); //Gets the layout parameters for the linear in the main screen so we can modify them in the code.
        ViewGroup.LayoutParams buttonScrollParams = buttonScroll.getLayoutParams(); //Ditto but for the scroll view.
        File questFile = new File(this.getFilesDir(), "quests.txt"); //Load the quests text file.
        if (!questFile.exists()){
            System.out.println("'quests.txt' does not exist! Creating file...");
            try {
                questFile.createNewFile(); //Create the quests.txt file if it does not exist.
                FileWriter tempwrite = new FileWriter(questFile, true);
                tempwrite.write("");
                tempwrite.close();
            }
            catch (Exception error){
                System.out.println("There was an error creating the quests.txt file!"); //If creating the quest.txt file caused an exception, print an error instead to avoid crashing.
                error.printStackTrace();
            }
        }
        String line = ""; //A string to hold lines as reading a line automatically makes the program move to the next.
        String buttonText = "";
        Integer questAmount = getQuestAmount(questFile); //Get the amount of quests in "quests.txt".
        Boolean inQuest = false; //A boolean to let the program know if it is currently reading a quest.
        boolean inQuestTasks = false; //Another boolean to let the program know if it is reading the tasks in a quest.
        boolean questFinished = false; //One more boolean to let the program know that the quest it was reading has finished.
        Button[] questButtons = new Button[questAmount]; //Creates an array that can hold 50 buttons.
        Scanner questFileScan = null; //Scanner to read the quest file. (Python! C++! Even GML! I miss you all!)
        try {
            questFileScan = new Scanner(questFile); //Creates a scanner object that is used to read the file... I miss C++ and Python already.
        }
        catch (Exception error){
            System.out.println("There was an error when opening the quests.txt file for reading!"); //Ditto to previous try statement.
            error.printStackTrace();
        }
        for (int i = 0; i < questAmount; System.out.println(i)){ //For loop that reads each quest in the quests file and creates a button for each that shows the name of the quest and completion status of the quest.
            try {
                if (questFileScan.hasNextLine()){ //Check if the file still has more lines.
                    System.out.println("Moving to next line");
                    line = questFileScan.nextLine(); //Save the current line to the line string and move to the next line. (There is no control on the latter part, it will move on to the next line no matter what you tell it!)
                    System.out.println("Current Line: " + line);
                }
                else{
                    System.out.println("Reached end of file!");
                    break; //Break out of the for loop because there are no more quests. (As the computer has reached the end of the file)
                }
                if (line.equals("StartQuest")){ //Check to see if the current line equals "StartQuest"
                    System.out.println("New Quest found in file!");
                    inQuest = true; //Set inQuest to true because a new quest was found.
                }
                if (!inQuest){ //This means to check if the inQuest variable is false. (As ! means "not" and when you call a boolean, it returns its value so == is not needed)
                    System.out.println("Not in quest");
                    if (questFileScan.hasNextLine()){
                        line = questFileScan.nextLine(); //Read the next line again if there are still more lines.
                    }
                    else{
                        break; //Break because there are no more quests.
                    }
                    if (line.equals("StartQuest")){ //StartQuest was found this second time so inQuest can now be set to true.
                        System.out.println("New Quest found in file! (2nd Chance)");
                        inQuest = true;
                    }
                    else{
                        System.out.println("New Quest NOT found in file! (2nd Chance)");
                        break; //StartQuest was not found for a second time and so the program will now assume that there are no more quests and will now break out of the for loop.
                    }
                }
                if (inQuest){ //Check if inQuest is true. (This is not an else statement as the previous if could make it true and, if it were an else, it would not be checked until the next loop.
                    System.out.println("Now in Quests");
                    if (line.equals("StartTasks")){ //If the line equals StartTasks, set inQuestTasks to true.
                        System.out.println("Now in Quest Tasks");
                        inQuestTasks = true;
                    }
                    if (line.equals("EndTasks")){ //If the line equals EndTasks, set inQuestTasks to false.
                        System.out.println("Now OUT OF Quest Tasks");
                        inQuestTasks = false;
                    }
                    if (!inQuestTasks){ //Check if inQuestTasks is false as we do not want to display the tasks on the quest buttons as it would be too verbose.
                        System.out.println("Not in Quest Tasks");
                        if (!line.equals("StartQuest") && !line.equals("StartTasks") && !line.equals("StartNewTask") && !line.equals("EndNewTask") && !line.equals("EndTasks") && !line.equals("EndTasks")){ //Prevent the commands from being added to buttons.
                            if (line.contains("id: ")){ //Check if the current line contains the quest id and save it to questID after removing the "id: " part.
                                System.out.println("Line contains ID");
                                questID = line.replace("id: ", "");
                            }
                            if (line.contains("name: ")){ //Ditto for the quest name.
                                System.out.println("Line contains name");
                                questName = line.replace("name: ", "");
                            }
                            if (line.contains("description: ")){ //Ditto for the quest description.
                                System.out.println("Line contains description");
                                questDescription = line.replace("description: ", "");
                            }
                            if (line.contains("status: ")){ //Ditto for the quest status.
                                System.out.println("Line contains status");
                                questStatus = line.replace("status: ", "");
                            }
                            if (!questID.equals("") && !questName.equals("") && !questDescription.equals("") && !questStatus.equals("") && !questFinished){ //Make sure none of the quest information is missing.
                                System.out.println("Creating new button");
                                buttonText = questName + "\n" + questStatus; //Save the quest name and status to the buttonText.
                                questButtons[i] = new Button(this); //Create a new button...
                                questButtons[i].setHeight(50); //...With a height of 50...
                                questButtons[i].setWidth(50);;//...And a width of 50.
                                if (questStatus.equals("Not Completed")) { //If the quest is not complete, make the button red, else, make it green.
                                    questButtons[i].setBackgroundColor(Color.RED);
                                    questButtons[i].setTextColor(Color.WHITE);
                                }
                                else{
                                    questButtons[i].setBackgroundColor(Color.GREEN);
                                    questButtons[i].setTextColor(Color.BLACK);
                                }
                                questButtons[i].setText(buttonText); //Make the button display the quest information.
                                questButtons[i].setId(Integer.parseInt(questID)); //Set the id of the button to the quest's id.
                                questButtons[i].setOnClickListener(questButtonPressed); //Set an listener for this button so that when it is pressed, it runs the function inside the listener.
                                LinearLayout.LayoutParams buttonLayout = new LinearLayout.LayoutParams(800, 300);
                                buttonLayout.setMargins(0, 0 ,0, 50); //Separate each button by 50.
                                questButtons[i].setLayoutParams(buttonLayout);
                                buttonLinear.addView(questButtons[i]); //Add the new button to the linear layout parented to the scroll view. (Scroll views automatically create linear layouts)
                                buttonLinear.setLayoutParams(buttonLinearParams);
                                buttonScroll.setLayoutParams(buttonScrollParams);
                                questFinished = true; //Mark the current quest as finished.
                            }
                            if (line.equals("EndQuest")){ //If the current line equals EndQuest, increase the i variable by one and set all quest information containers to be empty and set questFinished to false.
                                System.out.println("Quest finished");
                                i++;
                                inQuest = false;
                                questID = "";
                                questName = "";
                                questDescription = "";
                                questStatus = "";
                                questFinished = false;
                            }
                        }
                    }
                }
            }
            catch (Exception error){
                System.out.println("There was an error reading 'quests.txt'!");
                TextView temptextview = findViewById(R.id.main_textview_current_quests);
                temptextview.setText("You're probably the worst\nprogrammer\nin the world.");
                error.printStackTrace();
                break;
            }
        }
        questFileScan.close(); //Closes the file.

    }

    public void addQuestButton(View view){
        Intent intentAddQuestSwitch = new Intent(this, AddQuestActivity.class);
        startActivity(intentAddQuestSwitch); //Switch to the addQuest activity.
        finish(); //Finish this activity.
    }

    public void clearQuestsButton(View view){ //Open "quests.txt" and set it to be empty if it has any text inside and then reload this activity to reload the quest list.
        File questFile = new File(this.getFilesDir(), "quests.txt");
        if (questFile.exists()){
            try {
                FileWriter questFileClearWrite = new FileWriter(questFile);
                questFileClearWrite.write("");
                questFileClearWrite.close();
                Intent intentMainReload = new Intent(this, MainActivity.class);
                startActivity(intentMainReload);
                finish();
            } catch (Exception error) {
                error.printStackTrace();
            }
        }
    }

    public void areYouSureSwap(View view){
        ConstraintLayout areYouSureConstraint = findViewById(R.id.main_constraint_areyousure);
        ConstraintLayout mainLayoutConstraint = findViewById(R.id.main_constraint_main);
        File questFile = new File(this.getFilesDir(), "quests.txt");
        if (questFile.exists()){
            if (questFile.length() > 0) {
                if (areYouSureConstraint.getVisibility() == View.VISIBLE){
                    areYouSureConstraint.setVisibility(View.GONE);
                    mainLayoutConstraint.setVisibility(View.VISIBLE);
                }
                else{
                    areYouSureConstraint.setVisibility(View.VISIBLE);
                    mainLayoutConstraint.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) { //When this activity is created, run the readQuests function.
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        readQuests();
    }
}