package com.example.questcompassapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class EditQuestActivity extends AppCompatActivity {
    String questID = "";
    String taskList = "";
    String newQuestString = "";
    String questInfo = "";
    String questName = "";
    String questDescription = "";
    String questTasks = "";
    String oldQuestTasks = "";
    Context MainContext = this;
    CountDownTimer finishTimer;

    public String taskConverter(String tasks){
        String line = "";
        String questTasksConverted = "";
        Integer taskID = 0;
        try{
            Scanner taskRead = new Scanner(tasks);
            while (taskRead.hasNextLine()){
                line = taskRead.nextLine();
                System.out.println("\n(taskConverter): Current Line: " + line);
                if (!line.equals("")){
                    if (questTasksConverted.length() <= 0) {
                        System.out.println("(taskConverter): Converting task...");
                        questTasksConverted = "StartNewTask\ntaskid: " + String.valueOf(taskID) + "\n" + "taskname: " + line.replace("taskname: ", "") + "\ntaskstatus: Not Completed\nEndNewTask";
                        taskID++;
                    }
                    else{
                        questTasksConverted = questTasksConverted + "\nStartNewTask\ntaskid: " + String.valueOf(taskID) + "\n" + "taskname: " + line.replace("taskname: ", "") + "\ntaskstatus: Not Completed\nEndNewTask";
                        taskID++;
                    }
                }
            }
        }
        catch (Exception error){
            System.out.println("(taskConverter): There was an error converting tasks!");
            error.printStackTrace();
            return null;
        }
        return questTasksConverted;
    }

    public void editQuestBackButton(View view){
        Intent intentMainSwitch = new Intent(MainContext, MainActivity.class);
        startActivity(intentMainSwitch); //Return to the main menu. (MainActivity.java)
        finish();
    }

    public void editQuestClearTasks(View view){
        TextView editQuestTasks = findViewById(R.id.editquest_textbox_task_list);
        questTasks = "";
        editQuestTasks.setText(getResources().getString(R.string.app_addquest_notasks));
    }

    public void editQuestSaveTask(View view){
        TextView editQuestTaskBox = findViewById(R.id.editquest_textbox_task_list);
        TextView editQuestTaskDuplicate = findViewById(R.id.editquest_textview_add_task_duplicate);
        EditText editQuestTaskNameEntry = findViewById(R.id.editquest_textentry_task_title);
        Button editQuestBackButton = findViewById(R.id.editquest_button_task_backbutton);
        if (String.valueOf(editQuestTaskNameEntry.getText()).contains("¬") || String.valueOf(editQuestTaskNameEntry.getText()).contains("!") || String.valueOf(editQuestTaskNameEntry.getText()).contains("£") || String.valueOf(editQuestTaskNameEntry.getText()).contains("$") || String.valueOf(editQuestTaskNameEntry.getText()).contains("%") || String.valueOf(editQuestTaskNameEntry.getText()).contains("^") || String.valueOf(editQuestTaskNameEntry.getText()).contains("&") || String.valueOf(editQuestTaskNameEntry.getText()).contains("*") || String.valueOf(editQuestTaskNameEntry.getText()).contains("[") || String.valueOf(editQuestTaskNameEntry.getText()).contains("{") || String.valueOf(editQuestTaskNameEntry.getText()).contains("}") || String.valueOf(editQuestTaskNameEntry.getText()).contains("]") || String.valueOf(editQuestTaskNameEntry.getText()).contains(";") || String.valueOf(editQuestTaskNameEntry.getText()).contains("'") || String.valueOf(editQuestTaskNameEntry.getText()).contains("~") || String.valueOf(editQuestTaskNameEntry.getText()).contains("<") || String.valueOf(editQuestTaskNameEntry.getText()).contains(">") || String.valueOf(editQuestTaskNameEntry.getText()).contains("?") || String.valueOf(editQuestTaskNameEntry.getText()).contains("/") || String.valueOf(editQuestTaskNameEntry.getText()).contains("|") || String.valueOf(editQuestTaskNameEntry.getText()).contains("StartQuest") || String.valueOf(editQuestTaskNameEntry.getText()).contains("id: ") || String.valueOf(editQuestTaskNameEntry.getText()).contains("name: ") || String.valueOf(editQuestTaskNameEntry.getText()).contains("description: ") || String.valueOf(editQuestTaskNameEntry.getText()).contains("status: ") || String.valueOf(editQuestTaskNameEntry.getText()).contains("StartTasks") || String.valueOf(editQuestTaskNameEntry.getText()).contains("StartNewTask") || String.valueOf(editQuestTaskNameEntry.getText()).contains("EndNewTask") || String.valueOf(editQuestTaskNameEntry.getText()).contains("EndTasks") || String.valueOf(editQuestTaskNameEntry.getText()).contains("EndQuest")){
            System.out.println("Task name contains special character!");
            editQuestTaskDuplicate.setText("Please do not use special characters!");
            editQuestTaskDuplicate.setVisibility(View.VISIBLE);
        }
        else if (String.valueOf(editQuestTaskNameEntry.getText()).equals("") || String.valueOf(editQuestTaskNameEntry.getText()).equals(" ")){
            System.out.println("Task contains nothing!");
            editQuestTaskDuplicate.setText("Task name is empty!");
            editQuestTaskDuplicate.setVisibility(View.VISIBLE);
        }
        else{
            if (questTasks.equals("")){
                questTasks = String.valueOf(editQuestTaskNameEntry.getText());
                editQuestTaskBox.setText(questTasks.replaceAll("taskname: ", ""));
                editQuestTaskNameEntry.setText(getResources().getString(R.string.app_addquest_taskname));
                editQuestBackButton.performClick();
            }
            else{
                if (questTasks.contains(String.valueOf(editQuestTaskNameEntry.getText()))){
                    editQuestTaskDuplicate.setText(getResources().getString(R.string.app_addquest_task_duplicate));
                    editQuestTaskDuplicate.setVisibility(View.VISIBLE);
                }
                else {
                    questTasks = questTasks + "\n" + String.valueOf(editQuestTaskNameEntry.getText());
                    editQuestTaskBox.setText(questTasks.replaceAll("taskname: ", ""));
                    editQuestTaskDuplicate.setText(getResources().getString(R.string.app_addquest_task_duplicate));
                    editQuestBackButton.performClick();
                }
            }
        }
    }

    public void editQuestTaskSwap(View view){
        ConstraintLayout editQuestMainConstraint = findViewById(R.id.editquest_constraint_main);
        ConstraintLayout editQuestTaskConstraint = findViewById(R.id.editquest_constraint_addtask);
        if (editQuestTaskConstraint.getVisibility() == View.VISIBLE){
            editQuestTaskConstraint.setVisibility(View.GONE);
            editQuestMainConstraint.setVisibility(View.VISIBLE);
        }
        else{
            editQuestMainConstraint.setVisibility(View.GONE);
            editQuestTaskConstraint.setVisibility(View.VISIBLE);
        }
    }

    public void getQuestInfo(){
        String line = "";
        Boolean inQuest = false;
        File questFile = new File(MainContext.getFilesDir(), "quests.txt");
        try{
            Scanner questFileRead = new Scanner(questFile);
            System.out.println("\nReading 'quests.txt' for current quest info");
            while (questFileRead.hasNextLine()){
                line = questFileRead.nextLine();
                System.out.println("\nCurrent Line: " + line);
                if (line.equals(questID)){
                    System.out.println("Quest found!");
                    inQuest = true;
                }
                if (inQuest){
                    if (line.equals("EndQuest")){
                        System.out.println("Reached end of quest!");
                        inQuest = false;
                        break;
                    }
                    else{
                        System.out.println("Adding line to questInfo!");
                        questInfo = questInfo + line + "\n";
                    }
                }
                else{
                    System.out.println("Not in quest. No action taken.");
                }
            }
            System.out.println("\nquestInfo =\n" + questInfo);
            return;
        }
        catch (Exception error){
            System.out.println("There was an error getting quest info from 'quests.txt'!");
            error.printStackTrace();
        }
    }

    public void readQuestInfo(){
        try{
            String line = "";
            Scanner questInfoRead = new Scanner(questInfo);
            System.out.println("Reading questInfo to save individual vars!");
            while (questInfoRead.hasNextLine()){
                line = questInfoRead.nextLine();
                System.out.println("\nCurrent Line: " + line);
                if (line.contains("name: ") && !line.contains("taskname: ")){
                    questName = line;
                    System.out.println("Quest name found! (" + questName + ")");
                }
                if (line.contains("description: ")){
                    questDescription = line;
                    System.out.println("Quest description found! (" + questDescription + ")");
                }
                if (line.contains("taskname: ")){
                    System.out.println("New task found!");
                    oldQuestTasks = oldQuestTasks  + "\n" + line;
                }
            }
            System.out.println("Saved all quest information!\nAdding information to widgets!");
            EditText questTitleEntry = findViewById(R.id.editquest_textentry_quest_title);
            EditText questDescriptionEntry = findViewById(R.id.editquest_textentrybox_quest_description);
            TextView questTaskEntry = findViewById(R.id.editquest_textbox_task_list);
            questTitleEntry.setText(questName.replace("name: ", ""));
            questDescriptionEntry.setText(questDescription.replace("description: ", ""));
            questTaskEntry.setText(oldQuestTasks.replaceAll("taskname: ", ""));
            questTasks = oldQuestTasks;
        }
        catch (Exception error){
            System.out.println("There was an error reading questInfo!");
            error.printStackTrace();
        }
    }

    public void editQuestSaveEdit(View view){
        System.out.println("Saving quest edit...");
        String line = "";
        StringBuffer textBuffer = new StringBuffer();
        Boolean inQuest = false;
        File questFile = new File(MainContext.getFilesDir(), "quests.txt");
        try{
            System.out.println("Adding 'quests.txt' to buffer!");
            Scanner questFileRead = new Scanner(questFile);
            while (questFileRead.hasNextLine()){
                line = questFileRead.nextLine();
                System.out.println("\nCurrent Line: " + line);
                System.out.println("Adding line to textBuffer!");
                textBuffer.append(line);
                textBuffer.append("\n");
            }
        }
        catch (Exception error){
            System.out.println("There was an error adding 'quests.txt' to textBuffer!");
            error.printStackTrace();
            return;
        }
        System.out.println("All lines added to textbuffer!");
        EditText questTitleEntry = findViewById(R.id.editquest_textentry_quest_title);
        EditText questDescriptionEntry = findViewById(R.id.editquest_textentrybox_quest_description);
        TextView questTaskList = findViewById(R.id.editquest_textbox_task_list);
        TextView questEmptyError = findViewById(R.id.editquest_textview_save_error);
        if (questTitleEntry.getText().length() <= 0 || questDescriptionEntry.getText().length() <= 0 || questTaskList.getText().length() <= 0 || questTaskList.getText().equals("No Tasks!")) {
            System.out.println("One of the text boxes is empty!");
            questEmptyError.setText("Please fill in all information!");
            questEmptyError.setVisibility(View.VISIBLE);
        }
        newQuestString = questInfo.replace(questName, "name: " +String.valueOf(questTitleEntry.getText()));
        newQuestString = newQuestString.replace(questDescription, "description: " + String.valueOf(questDescriptionEntry.getText()));
        System.out.println("Converting tasks...");
        String oldQuestTasksConverted;
        String questTasksConverted;
        oldQuestTasksConverted = taskConverter(oldQuestTasks);
        questTasksConverted = taskConverter(questTasks);
        if (oldQuestTasksConverted == null || questTasksConverted == null){
            System.out.println("Error: Unable to convert tasks!");
            return;
        }
        System.out.println("\noldQuestTasksConverted =\n" + oldQuestTasksConverted);
        System.out.println("\nquestTasksConverted =\n" + questTasksConverted);
        if (newQuestString.contains(oldQuestTasksConverted)){
            System.out.println("\nTRUE!\n");
        }
        else{
            System.out.println("\nFALSE!\n");
        }
        newQuestString = newQuestString.replace(oldQuestTasksConverted, questTasksConverted);
        System.out.println("\nnewQuestString =\n" + newQuestString);
        System.out.println("\nCreated new version of quest. Replacing textBuffer with new edited quest...");
        String textBufferString = String.valueOf(textBuffer);
        System.out.println("\ntextBufferString =\n" + textBufferString);
        textBufferString = textBufferString.replace(questInfo, newQuestString);
        System.out.println("\ntextBufferString Converted =\n" + textBufferString);
        System.out.println("Replaced old quest with new edited quest!");
        System.out.println("\nSaving textBuffer to 'quests.txt'");
        try{
            FileWriter questFileWrite = new FileWriter(questFile);
            questFileWrite.write(textBufferString);
            questFileWrite.close();
            System.out.println("\nSuccessfully edited quest!\nReturning to main menu...");
        }
        catch (Exception error){
            System.out.println("There was an error saving the edited quest to 'quests.txt'");
            error.printStackTrace();
            return;
        }
        Intent IntentMainMenuSwap = new Intent(MainContext, MainActivity.class);
        startActivity(IntentMainMenuSwap);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_quest);
        Intent IntentGetQuestID = getIntent();
        questID = IntentGetQuestID.getStringExtra(QuestViewActivity.EDIT_MESSAGE);
        System.out.println(questID);
        getQuestInfo();
        readQuestInfo();
    }
}