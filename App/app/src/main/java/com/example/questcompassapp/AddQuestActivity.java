package com.example.questcompassapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class AddQuestActivity extends AppCompatActivity {
    String taskList = "";
    String newQuestString = "";
    CountDownTimer finishTimer;

    public void addQuestBackButton(View view){
        Intent intentMainSwitch = new Intent(this, MainActivity.class);
        startActivity(intentMainSwitch); //Return to the main menu. (MainActivity.java)
        finish();
    }

    public void addTaskSwapButton(View view){
        ConstraintLayout addQuestMainLayout = findViewById(R.id.addquest_constraint_main); //Swap between the add task and add quest views.
        ConstraintLayout addQuestAddTaskLayout = findViewById(R.id.addquest_constraint_addtask);
        TextView taskDuplicate = findViewById(R.id.addquest_textview_add_task_duplicate);
        if (addQuestAddTaskLayout.getVisibility() == View.VISIBLE){
            addQuestMainLayout.setVisibility(View.VISIBLE);
            addQuestAddTaskLayout.setVisibility(View.GONE);
        }
        else{
            addQuestMainLayout.setVisibility(View.GONE);
            addQuestAddTaskLayout.setVisibility(View.VISIBLE);
            taskDuplicate.setVisibility(View.GONE);
        }
    }

    public void addQuestClearTasks(View view){ //Reset the task list to be empty and also reset the task text box to say "No Tasks!" using the defined string called "app_addquest_notasks" in strings.xml.
        TextView taskBox = findViewById(R.id.addquest_textbox_task_list);
        taskList = "";
        taskBox.setText(getResources().getString(R.string.app_addquest_notasks));
    }

    public void addQuestSaveTask(View view){
        TextView taskBox = findViewById(R.id.addquest_textbox_task_list);
        TextView taskDuplicate = findViewById(R.id.addquest_textview_add_task_duplicate);
        EditText taskNameEntry = findViewById(R.id.addquest_textentry_task_title);
        Button taskBackButton = findViewById(R.id.addquest_button_task_backbutton);
        if (String.valueOf(taskNameEntry.getText()).contains("¬") || String.valueOf(taskNameEntry.getText()).contains("!") || String.valueOf(taskNameEntry.getText()).contains("£") || String.valueOf(taskNameEntry.getText()).contains("$") || String.valueOf(taskNameEntry.getText()).contains("%") || String.valueOf(taskNameEntry.getText()).contains("^") || String.valueOf(taskNameEntry.getText()).contains("&") || String.valueOf(taskNameEntry.getText()).contains("*") || String.valueOf(taskNameEntry.getText()).contains("[") || String.valueOf(taskNameEntry.getText()).contains("{") || String.valueOf(taskNameEntry.getText()).contains("}") || String.valueOf(taskNameEntry.getText()).contains("]") || String.valueOf(taskNameEntry.getText()).contains(";") || String.valueOf(taskNameEntry.getText()).contains("'") || String.valueOf(taskNameEntry.getText()).contains("~") || String.valueOf(taskNameEntry.getText()).contains("<") || String.valueOf(taskNameEntry.getText()).contains(">") || String.valueOf(taskNameEntry.getText()).contains("?") || String.valueOf(taskNameEntry.getText()).contains("/") || String.valueOf(taskNameEntry.getText()).contains("|") || String.valueOf(taskNameEntry.getText()).contains("StartQuest") || String.valueOf(taskNameEntry.getText()).contains("id: ") || String.valueOf(taskNameEntry.getText()).contains("name: ") || String.valueOf(taskNameEntry.getText()).contains("description: ") || String.valueOf(taskNameEntry.getText()).contains("status: ") || String.valueOf(taskNameEntry.getText()).contains("StartTasks") || String.valueOf(taskNameEntry.getText()).contains("StartNewTask") || String.valueOf(taskNameEntry.getText()).contains("EndNewTask") || String.valueOf(taskNameEntry.getText()).contains("EndTasks") || String.valueOf(taskNameEntry.getText()).contains("EndQuest")){
            System.out.println("Task contains special character!");
            taskDuplicate.setText("Please do not use special characters!");
            taskDuplicate.setVisibility(View.VISIBLE);
        }
        else if (String.valueOf(taskNameEntry.getText()).equals("") || String.valueOf(taskNameEntry.getText()).equals(" ")){
            System.out.println("Task contains nothing!");
            taskDuplicate.setText("Task name is empty!");
            taskDuplicate.setVisibility(View.VISIBLE);
        }
        else {
            if (taskList.equals("")) {
                taskList = String.valueOf(taskNameEntry.getText());
                taskBox.setText(taskList);
                taskNameEntry.setText(getResources().getString(R.string.app_addquest_taskname));
                taskBackButton.performClick();
            } else {
                if (taskList.contains(String.valueOf(taskNameEntry.getText()))) {
                    taskDuplicate.setText(getResources().getString(R.string.app_addquest_task_duplicate));
                    taskDuplicate.setVisibility(View.VISIBLE);
                } else {
                    taskList = taskList + "\n" + String.valueOf(taskNameEntry.getText());
                    taskBox.setText(taskList);
                    taskNameEntry.setText(getResources().getString(R.string.app_addquest_taskname));
                    taskBackButton.performClick();
                }
            }
        }
    }

    public void addQuestSaveQuest(View view){
        System.out.println("Creating new quest...");
        String newQuestID = "id: 0";
        String temp_value;
        int newQuestIDInt;
        Boolean questReadFailure = false;
        Boolean writeFailure = false;
        Boolean emptyTextBox = false;
        TextView questName = findViewById(R.id.addquest_textentry_quest_title);
        TextView questDescription = findViewById(R.id.addquest_textentrybox_quest_description);
        TextView questTasks = findViewById(R.id.addquest_textbox_task_list);
        TextView saveError = findViewById(R.id.addquest_textview_save_error);
        if (String.valueOf(questName.getText()).contains("¬") || String.valueOf(questName.getText()).contains("!") || String.valueOf(questName.getText()).contains("£") || String.valueOf(questName.getText()).contains("$") || String.valueOf(questName.getText()).contains("%") || String.valueOf(questName.getText()).contains("^") || String.valueOf(questName.getText()).contains("&") || String.valueOf(questName.getText()).contains("*") || String.valueOf(questName.getText()).contains("[") || String.valueOf(questName.getText()).contains("{") || String.valueOf(questName.getText()).contains("}") || String.valueOf(questName.getText()).contains("]") || String.valueOf(questName.getText()).contains(";") || String.valueOf(questName.getText()).contains("'") || String.valueOf(questName.getText()).contains("~") || String.valueOf(questName.getText()).contains("<") || String.valueOf(questName.getText()).contains(">") || String.valueOf(questName.getText()).contains("?") || String.valueOf(questName.getText()).contains("/") || String.valueOf(questName.getText()).contains("|") || String.valueOf(questName.getText()).contains("StartQuest") || String.valueOf(questName.getText()).contains("id: ") || String.valueOf(questName.getText()).contains("name: ") || String.valueOf(questName.getText()).contains("description: ") || String.valueOf(questName.getText()).contains("status: ") || String.valueOf(questName.getText()).contains("StartTasks") || String.valueOf(questName.getText()).contains("StartNewTask") || String.valueOf(questName.getText()).contains("EndNewTask") || String.valueOf(questName.getText()).contains("EndTasks") || String.valueOf(questName.getText()).contains("EndQuest")){
            System.out.println("Task contains special character!");
            saveError.setText("Please do not use special characters!");
            saveError.setVisibility(View.VISIBLE);
            return;
        }
        if (String.valueOf(questDescription.getText()).contains("¬") || String.valueOf(questDescription.getText()).contains("!") || String.valueOf(questDescription.getText()).contains("£") || String.valueOf(questDescription.getText()).contains("$") || String.valueOf(questDescription.getText()).contains("%") || String.valueOf(questDescription.getText()).contains("^") || String.valueOf(questDescription.getText()).contains("&") || String.valueOf(questDescription.getText()).contains("*") || String.valueOf(questDescription.getText()).contains("[") || String.valueOf(questDescription.getText()).contains("{") || String.valueOf(questDescription.getText()).contains("}") || String.valueOf(questDescription.getText()).contains("]") || String.valueOf(questDescription.getText()).contains(";") || String.valueOf(questDescription.getText()).contains("'") || String.valueOf(questDescription.getText()).contains("~") || String.valueOf(questDescription.getText()).contains("<") || String.valueOf(questDescription.getText()).contains(">") || String.valueOf(questDescription.getText()).contains("?") || String.valueOf(questDescription.getText()).contains("/") || String.valueOf(questDescription.getText()).contains("|") || String.valueOf(questDescription.getText()).contains("StartQuest") || String.valueOf(questDescription.getText()).contains("id: ") || String.valueOf(questDescription.getText()).contains("name: ") || String.valueOf(questDescription.getText()).contains("description: ") || String.valueOf(questDescription.getText()).contains("status: ") || String.valueOf(questDescription.getText()).contains("StartTasks") || String.valueOf(questDescription.getText()).contains("StartNewTask") || String.valueOf(questDescription.getText()).contains("EndNewTask") || String.valueOf(questDescription.getText()).contains("EndTasks") || String.valueOf(questDescription.getText()).contains("EndQuest")){
            System.out.println("Task contains special character!");
            saveError.setText("Please do not use special characters!");
            saveError.setVisibility(View.VISIBLE);
            return;
        }
        File questFile = new File(this.getFilesDir(), "quests.txt");
        if (questFile.length() <= 5) {
            System.out.println("There are no quests in the file. Setting new quest id to 0");
            newQuestID = "id: 0";
        } else {
            try {
                Scanner questFileRead = new Scanner(questFile);
                while (questFileRead.hasNextLine()) {
                    System.out.println("Reading 'quests.txt' to get new quest id");
                    temp_value = questFileRead.nextLine();
                    if (temp_value.contains("id: ") && !temp_value.contains("taskid: ")) {
                        newQuestID = temp_value;
                        System.out.println("Next id found!: " + newQuestID);
                    }
                }
            } catch (Exception error) {
                System.out.println("There was an error reading 'quests.txt' to get new quest ID!");
                error.printStackTrace();
                questReadFailure = true;
                saveError.setText("There was an error (Error: QREAD)");
                saveError.setVisibility(View.VISIBLE);
            }
        }
        if (questName.length() <= 0 || questDescription.length() <= 0 || questTasks.length() <= 0 || questTasks.getText().equals("No Tasks!")) {
            emptyTextBox = true;
            System.out.println("One of the text boxes is empty!");
            saveError.setText("Please fill in all information!");
            saveError.setVisibility(View.VISIBLE);
        }
        System.out.println("quesReadFailure = " + String.valueOf(questReadFailure));
        System.out.println("emptyTextBox = " + String.valueOf(emptyTextBox));
        if (!questReadFailure && !emptyTextBox) {
            System.out.println("Final id is: " + newQuestID);
            System.out.println("New quest id set, creating quest data...");
            newQuestID = newQuestID.replace("id: ", "");
            newQuestIDInt = Integer.valueOf(newQuestID);
            newQuestIDInt++;
            newQuestID = "id: " + String.valueOf(newQuestIDInt);
            System.out.println("New quest id is: " + newQuestID);
            newQuestString = "StartQuest\n";
            newQuestString = newQuestString + newQuestID + "\n";
            newQuestString = newQuestString + "name: " + questName.getText() + "\n";
            newQuestString = newQuestString + "description: " + questDescription.getText() + "\n";
            newQuestString = newQuestString + "status: Not Completed\n";
            newQuestString = newQuestString + "StartTasks\n";
            Scanner tasksRead = new Scanner(String.valueOf(questTasks.getText()));
            int taskIDCounter = 0;
            while (tasksRead.hasNextLine()) {
                System.out.println("Creating tasks...");
                newQuestString = newQuestString + "StartNewTask\n";
                newQuestString = newQuestString + "taskid: " + String.valueOf(taskIDCounter) + "\n";
                newQuestString = newQuestString + "taskname: " + tasksRead.nextLine() + "\n";
                newQuestString = newQuestString + "taskstatus: Not Completed\n";
                newQuestString = newQuestString + "EndNewTask\n";
                taskIDCounter++;
            }
            System.out.println("Tasks complete");
            newQuestString = newQuestString + "EndTasks\n";
            newQuestString = newQuestString + "EndQuest\n";
            System.out.println("New quest data complete");
            try {
                System.out.println("Saving quest data to 'quests.txt...");
                FileWriter questFileWrite = new FileWriter(questFile, true);
                questFileWrite.write(newQuestString);
                questFileWrite.close();
                System.out.println("Save complete");
            } catch (Exception error) {
                System.out.println("There was an error when writing to 'quests.txt'!");
                error.printStackTrace();
                writeFailure = true;
                saveError.setText("There was an error! (Error: QWRITE)");
                saveError.setVisibility(View.VISIBLE);
            }
            if (!writeFailure) {
                System.out.println("New quest creation complete, showing success message...");
                ConstraintLayout addQuestMainLayout = findViewById(R.id.addquest_constraint_main);
                ConstraintLayout addQuestComplete = findViewById(R.id.addquest_constraint_saved);
                addQuestMainLayout.setVisibility(View.GONE);
                addQuestComplete.setVisibility(View.VISIBLE);
                TextView countdownText = findViewById(R.id.addquest_textview_move_on_timer_countdown);
                ProgressBar finishProgressBar = findViewById(R.id.addquest_progressbar_moveon_timer);
                Context timerContext = this;
                finishTimer = new CountDownTimer(5000, 1000) {
                    @Override
                    public void onTick(long timerInt) {
                        finishProgressBar.setProgress(Math.toIntExact(timerInt));
                        countdownText.setText(String.valueOf(timerInt / 1000));
                    }

                    @Override
                    public void onFinish() {
                        Intent intentMainSwitch = new Intent(timerContext, MainActivity.class);
                        startActivity(intentMainSwitch);
                        finish();
                    }
                }.start();
            }
            System.out.println("Something seems to have gone wrong");
            saveError.setText("Something went wrong?");
            saveError.setVisibility(View.VISIBLE);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_quest);

    }
}