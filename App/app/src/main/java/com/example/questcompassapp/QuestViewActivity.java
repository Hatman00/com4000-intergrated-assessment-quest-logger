package com.example.questcompassapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileWriter;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class QuestViewActivity extends AppCompatActivity {
    String questID = "-999";
    LinearLayout taskButtonLinear;
    Button[] taskButtons;
    Context MainContext = this;
    public static final String EDIT_MESSAGE = "com.example.questcompassapp.MESSAGE";

    public void questCompleteCheck(){
        String line = "";
        String questInfo = "";
        Integer notCompletedAmount = 0;
        boolean inQuest = false;
        boolean inTasks = false;
        boolean getQuestInfo = false;
        File questFile = new File(MainContext.getFilesDir(), "quests.txt");
        try{
            Scanner questFileRead = new Scanner(questFile);
            System.out.println("Counting completed quests...");
            while (questFileRead.hasNextLine()){
                line = questFileRead.nextLine();
                System.out.println("\nCurrent Line: " + line);
                if (line.equals(questID)){
                    System.out.println("Quest found!");
                    inQuest = true;
                }
                if (inQuest){
                    if (line.equals("StartTasks")){
                        System.out.println("Tasks found!");
                        inTasks = true;
                    }
                    if (inTasks){
                        if (line.equals("taskstatus: Not Completed")){
                            notCompletedAmount++;
                            System.out.println("Found an undone task! Total is: " + String.valueOf(notCompletedAmount));
                        }
                        if (line.equals("EndTasks")){
                            inQuest = false;
                            inTasks = false;
                            break;
                        }
                    }
                }
            }
        }
        catch (Exception error){
            System.out.println("There was an error reading 'quests.txt' to check for quest completion!");
            error.printStackTrace();
        }
        if (notCompletedAmount <= 0){
            System.out.println("\nAll tasks complete! Marking quest as complete...");
            StringBuffer textBuffer = new StringBuffer();
            try{
                Scanner questFileRead = new Scanner(questFile);
                System.out.println("Adding 'quests.txt' to textBuffer");
                while (questFileRead.hasNextLine()){
                    line = questFileRead.nextLine();
                    System.out.println("\nCurrent Line: " + line);
                    if (line.equals(questID)){
                        System.out.println("Quest found!");
                        getQuestInfo = true;
                        inQuest = true;
                    }
                    if (line.equals("StartTasks")){
                        System.out.println("Start of tasks found, setting getQuestInfo to false!");
                        getQuestInfo = false;
                    }
                    if (getQuestInfo){
                        System.out.println("Adding line to questInfo");
                        questInfo = questInfo + line + "\n";
                    }
                    System.out.println("Adding line to textBuffer");
                    textBuffer.append(line);
                    textBuffer.append("\n");
                }
                System.out.println("Added all lines to textBuffer!\n\nReplacing quest completion status in textBuffer!");
                String newQuestInfo = questInfo.replace("status: Not Completed", "status: Completed");
                String textBufferString = String.valueOf(textBuffer);
                System.out.println("textBufferString =\n" + textBufferString + "\n");
                System.out.println("newQuestInfo =\n" + newQuestInfo);
                textBufferString = textBufferString.replace(questInfo, newQuestInfo);
                System.out.println("textBufferString replaced!\n\ntextBufferString =\n" + textBufferString);
                FileWriter questFileWrite = new FileWriter(questFile);
                questFileWrite.write(textBufferString);
                questFileWrite.close();
            }
            catch (Exception error){
                System.out.println("There was an error marking the quest as complete!");
                error.printStackTrace();
            }
        }
        else{
            System.out.println("\nAll tasks complete! Marking quest as complete...");
            StringBuffer textBuffer = new StringBuffer();
            try{
                Scanner questFileRead = new Scanner(questFile);
                System.out.println("Adding 'quests.txt' to textBuffer");
                while (questFileRead.hasNextLine()){
                    line = questFileRead.nextLine();
                    System.out.println("\nCurrent Line: " + line);
                    if (line.equals(questID)){
                        System.out.println("Quest found!");
                        getQuestInfo = true;
                        inQuest = true;
                    }
                    if (line.equals("StartTasks")){
                        System.out.println("Start of tasks found, setting getQuestInfo to false!");
                        getQuestInfo = false;
                    }
                    if (getQuestInfo){
                        System.out.println("Adding line to questInfo");
                        questInfo = questInfo + line + "\n";
                    }
                    System.out.println("Adding line to textBuffer");
                    textBuffer.append(line);
                    textBuffer.append("\n");
                }
                System.out.println("Added all lines to textBuffer!\n\nReplacing quest completion status in textBuffer!");
                String newQuestInfo = questInfo.replace("status: Completed", "status: Not Completed");
                String textBufferString = String.valueOf(textBuffer);
                System.out.println("textBufferString =\n" + textBufferString + "\n");
                System.out.println("newQuestInfo =\n" + newQuestInfo);
                textBufferString = textBufferString.replace(questInfo, newQuestInfo);
                System.out.println("textBufferString replaced!\n\ntextBufferString =\n" + textBufferString);
                FileWriter questFileWrite = new FileWriter(questFile);
                questFileWrite.write(textBufferString);
                questFileWrite.close();
            }
            catch (Exception error){
                System.out.println("There was an error marking the quest as not complete!");
                error.printStackTrace();
            }
        }
    }

    public void questViewBackButton(View view){
        Intent intentMainSwitch = new Intent(this, MainActivity.class);
        startActivity(intentMainSwitch); //Return to the main menu. (MainActivity.java)
        finish();
    }

    public Map getQuestInfo(String questID){
        String line = "";
        Map<String, String> questInfo = new HashMap<String, String>(); //This creates a dictionary of sorts.
        File questFile = new File(this.getFilesDir(), "quests.txt");
        try{
            Scanner questFileRead = new Scanner(questFile);
            while(questFileRead.hasNextLine()){
                line = questFileRead.nextLine();
                if (line.equals(questID)){
                    questInfo.put("questName", questFileRead.nextLine());
                    questInfo.put("questDescription", questFileRead.nextLine());
                    questInfo.put("questStatus", questFileRead.nextLine());
                }
            }
        }
        catch (Exception error){
            System.out.println("There was an error reading 'quests.txt' for quest information!");
            error.printStackTrace();
        }
        return questInfo;
    }

    public Map getQuestTasks(String questID){
        String line = "";
        Map<String, String> questTasks= new HashMap<String, String>();
        String taskID= "";
        String taskName= "";
        String taskStatus= "";
        Integer questTaskID = 0;
        Integer questTaskAmount = 0;
        Boolean questIDFound = false;
        Boolean questTasksFound = false;
        Boolean questReadTask = false;
        File questFile = new File(MainContext.getFilesDir(), "quests.txt");
        System.out.println("Reading 'quests.txt' for quest tasks...");
        try{
            Scanner questFileRead = new Scanner(questFile);
            while(questFileRead.hasNextLine()){
                line = questFileRead.nextLine();
                System.out.println("Current line is: " + line);
                if (line.equals(questID)){
                    System.out.println("Quest ID Found!");
                    questIDFound = true;
                }
                if (questIDFound) {
                    if (line.equals("StartTasks") && !questTasksFound) {
                        System.out.println("Start of Quest Tasks found!");
                        questTasksFound = true;
                    }
                    if (questTasksFound){
                        if (line.equals("StartNewTask")){
                            System.out.println("Found new Quest Task!");
                            questReadTask = true;
                        }
                        if (questReadTask){
                            questTaskAmount++;
                            System.out.println("Saving Quest Task to questTasks map!");
                            taskID = questFileRead.nextLine();
                            taskName = questFileRead.nextLine();
                            taskStatus = questFileRead.nextLine();
                            System.out.println("taskID: " + taskID.replace("taskid: ", ""));
                            System.out.println("taskName: " + taskName);
                            System.out.println("taskStatus: " + taskStatus);
                            questTasks.put("taskid(" + String.valueOf(questTaskID) + ")", taskID);
                            questTasks.put("taskname(" + String.valueOf(questTaskID) + ")", taskName);
                            questTasks.put("taskstatus(" + String.valueOf(questTaskID) + ")", taskStatus);
                            if (questTasks.containsKey("TaskAmount")){
                                questTasks.replace("TaskAmount", String.valueOf(questTaskAmount));
                            }
                            else{
                                questTasks.put("TaskAmount", String.valueOf(questTaskAmount));
                            }
                            questReadTask = false;
                            questTaskID++;
                            taskID = "";
                            taskName = "";
                            taskStatus = "";
                            System.out.println("Moving on to next quest");
                        }
                        if (line.equals("EndTasks")){
                            System.out.println("Reached the end of all Quest Tasks!");
                            break;
                        }
                    }
                }
            }
        }
        catch (Exception error){
            System.out.println("There was an error reading 'quests.txt' for quest information!");
            error.printStackTrace();
        }
        return questTasks;
    }

    View.OnClickListener completeTask = new View.OnClickListener() {
        public void onClick(View view) {
            String calling_button_id = String.valueOf(view.getId()); //Get the id of the button that has been pressed. (The id in this case is the same as the quest it displays)
            System.out.println("calling_button_id = " + calling_button_id);
            String taskId = "taskid: " + calling_button_id;
            System.out.println("taskId = " + taskId);
            String line = "";
            String questInfo = "";
            String taskName = "";
            String oldText = "";
            String newText = "";
            String newTask = "";
            String taskStatus = "Not Completed";
            StringBuffer textBuffer = new StringBuffer();
            Boolean inQuest = false;
            Boolean inTask = false;
            File questFile = new File(MainContext.getFilesDir(), "quests.txt");
            try {
                Scanner questRead = new Scanner(questFile);
                System.out.println("Adding 'quests.txt' to textBuffer");
                while (questRead.hasNextLine()) {
                    line = questRead.nextLine();
                    System.out.println("\nCurrent Line: " + line);
                    System.out.println("inQuest = " + String.valueOf(inQuest));
                    if (line.equals(questID)) {
                        System.out.println("Quest found!");
                        inQuest = true;
                    }
                    if (line.equals("EndQuest")) {
                        System.out.println("Reached end of quest!");
                        inQuest = false;
                    }
                    if (inQuest) {
                        System.out.println("Adding line to questInfo!");
                        questInfo = questInfo + line + "\n";
                        if (line.equals(taskId)) {
                            inTask = true;
                        }
                        if (inTask) {
                            if (line.equals("EndNewTask")) {
                                System.out.println("Reached end of current task!");
                                inTask = false;
                            } else {
                                System.out.println("Adding current task to oldText!");
                                oldText = oldText + line + "\n";
                                if (line.contains("taskname: ")) {
                                    taskName = line;
                                }
                                if (line.contains("taskstatus: ")) {
                                    taskStatus = line;
                                }
                            }
                        }
                    }
                    System.out.println("Adding line to textBuffer!");
                    textBuffer.append(line);
                    textBuffer.append("\n");
                }
            } catch (Exception error) {
                System.out.println("There was an error while adding 'quests.txt' to the textBuffer!");
                error.printStackTrace();
                return;
            }
            String textBufferString = String.valueOf(textBuffer);
            System.out.println("textBufferString =\n" + textBufferString + "\n");
            System.out.println("oldText =\n" + oldText + "\n");
            if (taskStatus.equals("taskstatus: Not Completed")) {
                System.out.println("Setting task to be completed");
                newText = oldText.replace("taskstatus: Not Completed", "taskstatus: Completed");
            } else {
                System.out.println("Setting task to be NOT completed");
                newText = oldText.replace("taskstatus: Completed", "taskstatus: Not Completed");
            }
            System.out.println("newText =\n" + newText + "\n");
            newTask = questInfo.replace(oldText, newText);
            System.out.println("newTask =\n" + newTask + "\n");
            textBufferString = textBufferString.replaceAll(questInfo, newTask);
            System.out.println("New textBufferString =\n" + textBufferString + "\n");
            try {
                System.out.println("Writing textBufferString to 'quests.txt'...");
                FileWriter questFileWrite = new FileWriter(questFile);
                questFileWrite.write(textBufferString);
                questFileWrite.close();
                if (taskStatus.equals("taskstatus: Not Completed")) {
                    newText = taskName.replace("taskname: ", "") + "\n" + "Completed";
                    taskButtons[Integer.parseInt(calling_button_id)].setBackgroundColor(Color.GREEN);
                    taskButtons[Integer.parseInt(calling_button_id)].setTextColor(Color.BLACK);
                } else {
                    newText = taskName.replace("taskname: ", "") + "\n" + "Not Completed";
                    taskButtons[Integer.parseInt(calling_button_id)].setBackgroundColor(Color.RED);
                    taskButtons[Integer.parseInt(calling_button_id)].setTextColor(Color.WHITE);
                }
                taskButtons[Integer.parseInt(calling_button_id)].setText(newText);
            } catch (Exception error) {
                System.out.println("There was an error when overwriting 'quests.txt'!");
                error.printStackTrace();
            }
            questCompleteCheck();
        }
    };

    public void deleteCurrentQuest(View view){
        File questFile = new File(MainContext.getFilesDir(), "quests.txt");
        String line = "";
        String questText = "";
        String textBufferString = "";
        String newQuestText = "";
        StringBuffer textBuffer = new StringBuffer();
        Boolean inQuest = false;
        try{
            Scanner questFileRead = new Scanner(questFile);
            System.out.println("Reading 'quests.txt' to find quest to delete.");
            while (questFileRead.hasNextLine()){
                line = questFileRead.nextLine();
                System.out.println("\nCurrent Line: " + line);
                if (line.equals(questID)){
                    System.out.println("Quest found!");
                    inQuest = true;
                }
                if (inQuest){
                    if (line.equals("EndQuest")){
                        System.out.println("End of quest found!");
                        inQuest = false;
                    }
                    else {
                        System.out.println("Saving quest text to questText");
                        questText = questText + line + "\n";
                    }
                }
                System.out.println("Adding line to textBuffer");
                textBuffer.append(line);
                textBuffer.append("\n");
            }
            System.out.println("Added all lines to buffer!");
        }
        catch (Exception error){
            System.out.println("There was an error reading 'quests.txt' while adding lines to buffer!");
            error.printStackTrace();
        }
        System.out.println("textBuffer = \n" + textBuffer);
        questText = "StartQuest\n" + questText + "EndQuest";
        textBufferString = String.valueOf(textBuffer);
        newQuestText = textBufferString.replace(questText, "");
        newQuestText = newQuestText.trim();
        System.out.println("\nnewQuestText = \n" + newQuestText);
        try{
            FileWriter questFileWrite = new FileWriter(questFile);
            Scanner newQuestTextRead = new Scanner(newQuestText);
            while (newQuestTextRead.hasNextLine()){
                line = newQuestTextRead.nextLine();
                System.out.println("Current Line: " + line);
                if (!line.isEmpty()){
                    System.out.println("Line is not empty. Writing to file");
                    questFileWrite.write(line);
                    questFileWrite.write("\n");
                }
            }
            questFileWrite.close();
        }
        catch (Exception error){
            System.out.println("There was an error overwriting 'quests.txt' with deleted quest!");
            error.printStackTrace();
        }
        Intent IntentMainSwitch = new Intent(MainContext, MainActivity.class);
        startActivity(IntentMainSwitch);
        finish();
    }

    public void questViewAreYouSureSwap(View view){
        ConstraintLayout questAreYouSureConstraint = findViewById(R.id.questview_constraint_areyousure);
        ConstraintLayout questMainLayoutConstraint = findViewById(R.id.questview_constraint_main);
        if (questAreYouSureConstraint.getVisibility() == View.VISIBLE){
            questAreYouSureConstraint.setVisibility(View.GONE);
            questMainLayoutConstraint.setVisibility(View.VISIBLE);
        }
        else{
            questAreYouSureConstraint.setVisibility(View.VISIBLE);
            questMainLayoutConstraint.setVisibility(View.GONE);
        }
    }

    public void EditQuestButton(View view){
        Intent intentQuestViewSwitch = new Intent(MainContext, EditQuestActivity.class); //Create intent to link this file and the QuestViewActivity
        intentQuestViewSwitch.putExtra(EDIT_MESSAGE, questID); //Add the button id to the intent as a message.
        startActivity(intentQuestViewSwitch); //Move to the EditQuestActivity file and send the message.
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quest_view);
        Intent intentCheck = getIntent();
        questID = intentCheck.getStringExtra(MainActivity.EXTRA_MESSAGE);
        String taskText = "";
        TextView questTitle = findViewById(R.id.questview_textview_questname);
        TextView questDescription = findViewById(R.id.questview_textview_questdescription);
        questID = "id: " + questID;
        Map questInfo = getQuestInfo(questID);
        questTitle.setText(String.valueOf(questInfo.get("questName")).replace("name: ", ""));
        questDescription.setText(String.valueOf(questInfo.get("questDescription")).replace("description: ", ""));
        Map questTasks = getQuestTasks(questID);
        Integer taskIDCounter = 0;
        taskButtons = new Button[Integer.parseInt(String.valueOf(questTasks.get("TaskAmount")))];
        taskButtonLinear = (LinearLayout) findViewById(R.id.questview_linear_taskbuttons);
        while (taskIDCounter <= Integer.parseInt(String.valueOf(questTasks.get("TaskAmount"))) - 1) {
            taskText = String.valueOf(questTasks.get("taskname(" + String.valueOf(taskIDCounter) + ")") + "\n" + String.valueOf(questTasks.get("taskstatus(" + String.valueOf(taskIDCounter) + ")")));
            taskText = taskText.replace("taskname: ", "");
            taskText = taskText.replace("taskstatus: ", "");
            System.out.println(questTasks.get("taskid(" + String.valueOf(taskIDCounter) + ")"));
            System.out.println(questTasks.get("taskname(" + String.valueOf(taskIDCounter) + ")"));
            System.out.println(questTasks.get("taskstatus(" + String.valueOf(taskIDCounter) + ")"));
            System.out.println("\n");
            taskButtons[taskIDCounter] = new Button(this);
            taskButtons[taskIDCounter].setHeight(50);
            taskButtons[taskIDCounter].setWidth(50);
            if (questTasks.get("taskstatus(" + String.valueOf(taskIDCounter) + ")").equals("taskstatus: Not Completed")){
                taskButtons[taskIDCounter].setBackgroundColor(Color.RED);
                taskButtons[taskIDCounter].setTextColor(Color.WHITE);
            }
            else{
                taskButtons[taskIDCounter].setBackgroundColor(Color.GREEN);
                taskButtons[taskIDCounter].setTextColor(Color.BLACK);
            }
            taskButtons[taskIDCounter].setText(taskText);
            taskButtons[taskIDCounter].setId(taskIDCounter);
            taskButtons[taskIDCounter].setOnClickListener(completeTask);
            LinearLayout.LayoutParams taskButtonLayout = new LinearLayout.LayoutParams(800, 300);
            taskButtonLayout.setMargins(0, 0, 0, 50);
            taskButtons[taskIDCounter].setLayoutParams(taskButtonLayout);
            taskButtonLinear.addView(taskButtons[taskIDCounter]);
            taskIDCounter++;
        }
        //TextView blah = findViewById(R.id.textView);
        //blah.setText(intentCheck.getStringExtra(MainActivity.EXTRA_MESSAGE));
    }
}